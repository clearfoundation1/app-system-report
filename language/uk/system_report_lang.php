<?php

$lang['system_report_app_description'] = 'Додаток «Системний звіт» містить інформацію про операційну систему та базове обладнання.';
$lang['system_report_app_name'] = 'Системний звіт';
$lang['system_report_available'] = 'Доступно';
$lang['system_report_cpu_model'] = 'Модель ЦП';
$lang['system_report_filesystem'] = 'Файлова система';
$lang['system_report_filesystem_summary'] = 'Інформація про файлову систему';
$lang['system_report_item'] = 'Пункт';
$lang['system_report_kernel_version'] = 'Версія ядра';
$lang['system_report_load'] = 'Навантаження';
$lang['system_report_memory_size'] = 'Розмір пам`яті';
$lang['system_report_mounted'] = 'Змонтовано';
$lang['system_report_size'] = 'Розмір';
$lang['system_report_system_details'] = 'Інформація про систему';
$lang['system_report_system_time'] = 'Системний час';
$lang['system_report_uptime'] = 'Час роботи';
$lang['system_report_use'] = 'Використ. %';
$lang['system_report_used'] = 'Використ.';
$lang['system_report_value'] = 'Значення';
